import React from 'react';
import {StyleSheet, Text} from "react-native";
import {ChessPiece, PieceColor} from "../types";

const types = {
    p: '♟',
    r: '♜',
    n: '♞',
    b: '♝',
    q: '♛',
    k: '♚',
};

const styles = StyleSheet.create({
    text: {
        fontSize: 40,
    },

})

export default function Piece(props: { piece: ChessPiece, rotate: boolean }) {
    return (
        <Text
            style={[styles.text, {
                color: props.piece.color === PieceColor.white ? '#fff' : '#000',
                transform: [{rotate: props.rotate ? '0deg' : '180deg'}]
            }]}
        >{types[props.piece.type]}</Text>
    );
}
