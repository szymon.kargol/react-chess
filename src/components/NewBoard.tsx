import {Move} from "../types";
import {Button, Text, TouchableOpacity, View} from "react-native";
import Piece from "./Piece";
import React, {useEffect, useState} from "react";
import Chess from "../chess/Chess";

type BoardProps = {
    sendMove: (move: Move) => void,
    color: 'white' | 'black',
    moves: Array<Move>,
}

const chess = new Chess();

export default function NewBoard(props: BoardProps) {

    const [selectedIndex, setSelectedIndex] = useState<number | null>(null);
    const [possibleFields, setPossibleFields] = useState<number[]>([]);
    const [currentMove, setCurrentMove] = useState<number>(-1);

    useEffect(() => {

    }, []);


    function onPressHandler(index: number): void {
        chess.goToLastMove()
        setCurrentMove(chess.getCurrentMove())
        const x88index = i88x(index);
        setPossibleFields([]);
        if (selectedIndex === null) {
            const piece = chess.pick(x88index);
            if (piece) {
                setPossibleFields(chess.generateMoves().filter(el => el.startIndex === x88index).map(el => el.targetIndex));
                setSelectedIndex(x88index);
            }
            return;
        }

        chess.move(selectedIndex, x88index)
        setCurrentMove(chess.getCurrentMove())
        setSelectedIndex(null);
        setPossibleFields([]);
    }

    function i88x(index88: number): number {
        return index88 + (index88 & ~7);
    }

    function findPiece(index: number) {
        const piece = chess.pick(index + (index & ~7));

        if (piece) {
            return (<Piece
                rotate={props.color === 'white'}
                piece={piece}/>);
        }

        return null;
    }

    function goTo() {
        chess.goTo(chess.getCurrentMove() - 1)
        setCurrentMove(chess.getCurrentMove())
    }

    return (
        <View
            style={{
                flex: 1,
                flexWrap: 'wrap',
                flexDirection: 'row',
                width: 400,
                backgroundColor: 'gray',
                alignContent: 'center',
                transform: [{rotate: props.color === 'white' ? '0' : '180deg'}],
            }}
        >
            {[...Array(64)].map((_, index) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={.7}
                            onPress={() => onPressHandler(index)}
                            key={`field_${index}`}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: (
                                    (index % 16) === 0 ||
                                    (index % 16 - 2) === 0 ||
                                    (index % 16 - 4) === 0 ||
                                    (index % 16 - 6) === 0 ||
                                    (index % 16 - 9) === 0 ||
                                    (index % 16 - 11) === 0 ||
                                    (index % 16 - 13) === 0 ||
                                    (index % 16 - 15) === 0
                                ) ? '#936b1a' : '#d2cbcb',
                                width: 50,
                                height: 50,
                                borderColor: 'blue',
                                borderWidth: i88x(index) === selectedIndex ? 3 : 0,
                            }}
                        >
                            {findPiece(index)}
                            <Text style={{
                                fontSize: 10,
                                position: "absolute",
                                textShadowColor: "white",
                                textShadowOffset: {width: 1, height: 1}
                            }}>
                            </Text>
                            {possibleFields.indexOf(i88x(index)) != -1 ?
                                <Text style={{position: "absolute", bottom: 0, color: "red", fontSize: 50}}>▢</Text> : null}
                        </TouchableOpacity>
                    )
                }
            )}

            <Text>
                move number: {currentMove}
            </Text>
            <Button title={'Prev'} onPress={goTo}/>
        </View>
    );
}
