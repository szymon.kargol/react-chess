import React from 'react';
import {Button, StyleSheet, Text, View} from "react-native";
import {Views} from "../types";

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#135d8f',
        marginBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnContainer: {
        marginBottom: 5,
    }
});

type MenuProps = {
    navigate: (view: Views) => void,
}

export default function Menu(props: MenuProps) {

    function handler_NewGame() {
        props.navigate('lobby');
    }

    function handler_History() {
        props.navigate('history');
    }

    return (
        <View style={styles.container}>
            <View style={styles.btnContainer}>
                <Button
                    title={'Play'}
                    onPress={handler_NewGame}
                    color='green'/>
            </View>
            <View style={styles.btnContainer}>
                <Button
                    title={'History'}
                    onPress={handler_History}
                    color='gray'/>
            </View>
        </View>
    );
}
