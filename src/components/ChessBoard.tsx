import {PieceColor} from "../types";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React, {forwardRef, Ref, useEffect, useImperativeHandle, useState} from "react";
import Chess from "../Chess";
import Piece from "./Piece";

type BoardProps = {
    sendMove: (move: string, pgn: string) => void,
    game: any,
    id: any,
    color: 'w' | 'b',
    ref: Ref<any>,
}

const chess = Chess();

function useForceUpdate() {
    const [_forceUpdate, set_forceUpdate] = useState(0);
    return () => set_forceUpdate(value => value + 1);
}

const ChessBoard = forwardRef((props: BoardProps, ref: Ref<any>) => {
    const forceUpdate = useForceUpdate();
    useImperativeHandle(ref, () => ({
        rec: (event: any) => {
            chess.move(event, {sloppy: true});
            setGameOver(chess.game_over());
            forceUpdate();
        },
        clock: (clock: any) => {
            console.log(clock)
            setLocalClock(props.game.player_one === props.id ? clock.one_time : clock.two_time)
            setRemoteClock(props.game.player_one === props.id ? clock.two_time : clock.one_time)
            startClock(props.game.player_one === props.id ? clock.one_time : clock.two_time, props.game.player_one === props.id ? clock.two_time : clock.one_time);
            // setGameOver(chess.game_over());
            forceUpdate();
        },
    }));

    const [selectedPlace, setSelectedPlace] = useState<string>('');
    const [possibleFields, setPossibleFields] = useState<Array<string>>([]);
    const [gameOver, setGameOver] = useState<boolean>(false);
    const [localClock, setLocalClock] = useState<number>(0);
    const [remoteClock, setRemoteClock] = useState<number>(0);
    const [currentInterval, setCurrentInterval] = useState<any>(null);

    useEffect(function() {
        setLocalClock(props.game.player_one === props.id ? props.game.one_time : props.game.two_time)
        setRemoteClock(props.game.player_one === props.id ? props.game.two_time : props.game.one_time)
        startClock(props.game.clock, props.game.clock)
    }, []);

    function startClock(localClock: any, remoteClock: any) {
        console.log('interval', localClock, remoteClock)
        clearInterval(currentInterval);
        const int = setInterval(() => {
            console.log('interval')
            if (chess.turn() === (props.color === 'w' ? 'w' : 'b')) {
                localClock -= 1000;
                setLocalClock(localClock);
            } else {
                remoteClock -= 1000;
                setRemoteClock(remoteClock);
            }
        }, 1000)
        setCurrentInterval(int);
    }

    function onPressHandler(index: number): void {
        if (props.color.substr(0, 1) !== chess.turn()) {
            return;
        }
        if (placeByIndex(index) === selectedPlace) {
            setSelectedPlace('');
            setPossibleFields([]);
            return;
        }

        if (possibleFields.length === 0) {
            const moves = chess.moves({square: placeByIndex(index)}) as Array<string>;
            console.log(moves);
            if (moves.length > 0) {
                setPossibleFields(moves);
                setSelectedPlace(placeByIndex(index));
            }
            return;
        }

        if (isPossible(index)) {
            const move = `${selectedPlace}${placeByIndex(index)}`;
            chess.move(move, {sloppy: true});
            setGameOver(chess.game_over());
            props.sendMove(move, chess.pgn());
            setPossibleFields([]);
            setSelectedPlace('');
        }
    }

    function findPiece(sq8x8: number) {
        const p = chess.get(placeByIndex(sq8x8));

        if (p) {
            const cp = {
                index: sq8x8,
                type: p.type,
                color: p.color === 'w' ? PieceColor.white : PieceColor.black,
                x: 0,
                y: 0,
            }

            return (<Piece
                rotate={props.color === 'w'}
                piece={cp}/>);
        }

        return null;
    }

    function placeByIndex(sq8x8: number): string {
        const file07 = sq8x8 % 8;
        const rank07 = 7 - Math.floor(sq8x8 / 8);
        const file = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

        return `${file[file07]}${rank07 + 1}`;
    }

    function isPossible(index: number) {
        const place = placeByIndex(index);
        if (chess.turn() === 'w' && (place === 'c1' && possibleFields.some(el => (el === 'O-O-O'))) || (place === 'g1' && possibleFields.some(el => (el === 'O-O')))) {
            return true;
        }

        if (chess.turn() === 'b' && (place === 'c8' && possibleFields.some(el => (el === 'O-O-O'))) || (place === 'g8' && possibleFields.some(el => (el === 'O-O')))) {
            return true;
        }


        return possibleFields.filter(el => el.includes(place)).length === 1;
    }

    function whoWon() {
        return chess.turn() === 'w' ? 'Black' : 'White';
    }

    function gameOverElement() {
        if (gameOver) {
            return (<Text>Game over! {whoWon()} wins!</Text>);
        }

        return;
    }

    function formatClock(clock: number): string {
        return (Math.floor(clock / 1000 / 60)) + ':' + ((clock / 1000 % 60) + '0').substr(0, 2);
    }

    return (
        <View style={styles.container}>
            {gameOverElement()}
            <Text
                style={styles.title}>{chess.turn() === (props.color === 'b' ? 'w' : 'b') ? '⏲' : '🤔'} {formatClock(remoteClock)}</Text>
            <View
                style={{
                    flex: 1,
                    flexWrap: 'wrap',
                    flexDirection: 'row',
                    width: 400,
                    alignContent: 'center',
                    transform: [{rotate: props.color === 'w' ? '0deg' : '180deg'}],
                }}
            >
                {[...Array(64)].map((_, index) => {
                        return (
                            <TouchableOpacity
                                activeOpacity={.7}
                                onPress={() => onPressHandler(index)}
                                key={`field_${index}`}
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: (
                                        (index % 16) === 0 ||
                                        (index % 16 - 2) === 0 ||
                                        (index % 16 - 4) === 0 ||
                                        (index % 16 - 6) === 0 ||
                                        (index % 16 - 9) === 0 ||
                                        (index % 16 - 11) === 0 ||
                                        (index % 16 - 13) === 0 ||
                                        (index % 16 - 15) === 0
                                    ) ? '#936b1a' : '#d2cbcb',
                                    width: 50,
                                    height: 50,
                                    borderColor: 'blue',
                                    // borderWidth: i88x(index) === selectedIndex ? 3 : 0,
                                }}
                            >
                                {findPiece(index)}
                                {/*<Text style={{*/}
                                {/*    fontSize: 10,*/}
                                {/*    position: "absolute",*/}
                                {/*    textShadowColor: "white",*/}
                                {/*    textShadowOffset: {width: 1, height: 1}*/}
                                {/*}}>*/}
                                {/*    {placeByIndex(index)}*/}
                                {/*</Text>*/}
                                {isPossible(index) ?
                                    <Text style={{
                                        position: "absolute",
                                        bottom: 0,
                                        color: "red",
                                        fontSize: 50
                                    }}>▢</Text> : null}
                            </TouchableOpacity>
                        )
                    }
                )}
            </View>
            <Text
                style={styles.title}>{chess.turn() === (props.color === 'w' ? 'w' : 'b') ? '⏲' : '🤔'} {formatClock(localClock)}</Text>
        </View>
    );
})
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#135d8f',
        marginBottom: 20,
        marginTop: 20,
    },
});


export default ChessBoard;
