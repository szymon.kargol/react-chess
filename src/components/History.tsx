import {StyleSheet, Text, View} from "react-native";
import React, {useState} from "react";
import HistoryBoard from "./HistoryBoard";

type HistoryProps = {
    games: Array<{ game_id: string, pgn: string }>,
    players: any,
}

export default function History(props: HistoryProps) {

    const [pgn, setPgn] = useState<string>('');

    function preview(game: any) {
        console.log(game)
        setPgn(game.pgn);
    }

    function getOpponentName(game: any) {
        return props.players.filter(player => player.id === game.player_one || player.id === game.player_two)[0].name
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Past games ({props.games.filter(game => Boolean(game.pgn)).length}):</Text>
            {props.games.filter(game => Boolean(game.pgn)).map((game: any) => {
                return (<Text key={game.game_id} onPress={() => preview(game)}>Game from {(new Date(game.one_move_time)).toDateString()} with {getOpponentName(game)}</Text>);
            })}
            <HistoryBoard pgn={pgn}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#135d8f',
        marginBottom: 20,
    },
});
