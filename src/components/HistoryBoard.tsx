import {PieceColor} from "../types";
import {Button, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React, {useEffect, useState} from "react";
import Piece from "./Piece";
import Chess from "../Chess";

type HistoryBoardProps = {
    pgn: string
}

const chess = Chess();

const pgn = [
    '[Event "Casual Game"]',
    '[Site "Berlin GER"]',
    '[Date "1852.??.??"]',
    '[EventDate "?"]',
    '[Round "?"]',
    '[Result "1-0"]',
    '[White "Adolf Anderssen"]',
    '[Black "Jean Dufresne"]',
    '[ECO "C52"]',
    '[WhiteElo "?"]',
    '[BlackElo "?"]',
    '[PlyCount "47"]',
    '',
    '1.e4 e5 2.Nf3 Nc6 3.Bc4 Bc5 4.b4 Bxb4 5.c3 Ba5 6.d4 exd4 7.O-O',
    'd3 8.Qb3 Qf6 9.e5 Qg6 10.Re1 Nge7 11.Ba3 b5 12.Qxb5 Rb8 13.Qa4',
    'Bb6 14.Nbd2 Bb7 15.Ne4 Qf5 16.Bxd3 Qh5 17.Nf6+ gxf6 18.exf6',
    'Rg8 19.Rad1 Qxf3 20.Rxe7+ Nxe7 21.Qxd7+ Kxd7 22.Bf5+ Ke8',
    '23.Bd7+ Kf8 24.Bxe7# 1-0'
]
export default function HistoryBoard(props: HistoryBoardProps) {

    const [moveNo, setMoveNo] = useState<number>(0);
    const [history, setHistory] = useState<Array<any>>([]);
    const [isAutoplay, setIsAutoplay] = useState<boolean>(false);

    useEffect(() => {
        chess.load_pgn(props.pgn);
        // chess.load_pgn(props.pgn);
        setHistory(chess.history());
        setMoveNo(chess.history().length);
    }, [props]);

    useEffect(() => {
        if (!isAutoplay) {
            return;
        }

        const play = () => {
            if (!isAutoplay) {
                return;
            }
            console.log('play');
            forward();
            if (moveNo === history.length) {
                console.log('xd');
                setIsAutoplay(false);
                return;
            }

            setTimeout(() => {
                play();
            }, 500)
        }
        play();
    }, ['isAutoplay'])

    function undo() {
        if (moveNo === 0) {
            return;
        }

        chess.undo();
        setMoveNo(moveNo - 1);
    }

    function forward() {
        if (moveNo === history.length) {
            return;
        }

        chess.move(history[moveNo], {sloppy: true});
        setMoveNo(moveNo + 1);
    }

    function toStart() {
        // @ts-ignore
        chess.load((new Chess).fen());
        setMoveNo(0);
    }

    function toEnd() {
        chess.load_pgn(pgn.join('\n'));
        // chess.load_pgn(props.pgn);
        setHistory(chess.history());
        setMoveNo(chess.history().length);
    }

    async function autoplay() {
        setIsAutoplay(!isAutoplay);
    }

    function placeByIndex(sq8x8: number): string {
        const file07 = sq8x8 % 8;
        const rank07 = 7 - Math.floor(sq8x8 / 8);
        const file = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

        return `${file[file07]}${rank07 + 1}`;
    }

    function findPiece(sq8x8: number) {
        const p = chess.get(placeByIndex(sq8x8));

        if (p) {
            const cp = {
                index: sq8x8,
                type: p.type,
                color: p.color === 'w' ? PieceColor.white : PieceColor.black,
                x: 0,
                y: 0,
            }

            return (<Piece
                rotate={true}
                piece={cp}/>);
        }

        return null;
    }

    return (
        <View style={styles.container}>
            <View
                style={{
                    flex: 1,
                    flexWrap: 'wrap',
                    flexDirection: 'row',
                    width: 400,
                    alignContent: 'center',
                }}
            >
                {[...Array(64)].map((_, index) => {
                        return (
                            <TouchableOpacity
                                key={`field_${index}`}
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: (
                                        (index % 16) === 0 ||
                                        (index % 16 - 2) === 0 ||
                                        (index % 16 - 4) === 0 ||
                                        (index % 16 - 6) === 0 ||
                                        (index % 16 - 9) === 0 ||
                                        (index % 16 - 11) === 0 ||
                                        (index % 16 - 13) === 0 ||
                                        (index % 16 - 15) === 0
                                    ) ? '#936b1a' : '#d2cbcb',
                                    width: 50,
                                    height: 50,
                                    borderColor: 'blue',
                                }}
                            >
                                {findPiece(index)}
                            </TouchableOpacity>
                        )
                    }
                )}
                <View
                    style={{
                        width: '100%',
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: 10
                    }}
                >
                    <Button title={' << '} onPress={() => toStart()}/>
                    <Button title={' < '} onPress={() => undo()}/>
                    {/*<Button title={' Autoplay '} onPress={() => autoplay()}/>*/}
                    <Button title={' > '} onPress={() => forward()}/>
                    <Button title={' >> '} onPress={() => toEnd()}/>
                </View>
                <View>
                    <Text style={{
                        width: 400
                    }}>
                        {history.map((el, i) => {
                            const vv = (i % 2 === 0) ? (i + 1) + '. ' + el : el;
                            if (i + 1 === moveNo) {
                                return (<Text key={`field_${i}`} style={{fontWeight: 'bold'}}>{vv} </Text>);
                            }
                            return `${vv} `;
                        })}
                    </Text>
                </View>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
