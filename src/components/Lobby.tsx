import React, {useState} from 'react';
import {Button, Picker, StyleSheet, Text, View} from "react-native";
import {Views} from "../types";

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#135d8f',
        marginBottom: 20,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnContainer: {
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    playersContainer: {},
    input: {
        borderWidth: 1,
        fontSize: 20,
    },
    player: {
        fontSize: 18,
    },
});

type LobbyProps = {
    navigate: (view: Views) => void,
    invite: (playerName: string | null, clock: number) => void,
    play: (game: any) => void,
    players: Array<any>,
    games: Array<any>,
    playerName: string,
}

export default function Lobby(props: LobbyProps) {

    const [playerName, setPlayerName] = useState(null);
    const [selectedClock, setClock] = useState(600000);

    function handler_Invite() {
        props.invite(playerName, selectedClock);
    }

    function play(game: any) {
        props.play(game);
    }

    function getOpponentName(game: any) {
        return props.players.filter(player => player.id === game.player_one || player.id === game.player_two)[0].name
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Invite player ({props.players.length} online)</Text>
            <Picker
                selectedValue={playerName}
                onValueChange={(itemValue) => setPlayerName(itemValue)}
                prompt={'Select opponent'}
                style={styles.player}
            >
                <Picker.Item key={'zero'} label={'Select one...'} value={'nullhehe'}/>
                {props.players.map((player: any) => {
                    return (<Picker.Item key={player.name} label={player.name} value={player.name}/>);
                })}
            </Picker>
            <View style={styles.btnContainer}>
                <Picker
                    selectedValue={selectedClock}
                    onValueChange={(itemValue) => setClock(itemValue)}
                    prompt={'Time control (clock)'}
                >
                    <Picker.Item label={'Time: Blitz (3)'} value={180000}/>
                    <Picker.Item label={'Time: Rapid (10)'} value={600000}/>
                    <Picker.Item label={'Time: Blitz (30)'} value={1800000}/>
                </Picker>
                <Button
                    title={'Invite'}
                    onPress={handler_Invite}
                    color='green'/>
            </View>
            <Text style={styles.title}>{props.games.length ? 'Your games in progress' : ''}</Text>
            <View style={styles.playersContainer}>
                {props.games.map((game: any) => {
                    return (
                        <View key={game.game_id}>
                            <Button
                                title={'Game from ' + (new Date(game.one_move_time)).toDateString() +' with '+ getOpponentName(game)}
                                onPress={() => play(game)}/>
                        </View>
                    );
                })}
            </View>
        </View>
    );
}
