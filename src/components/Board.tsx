import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import Piece from "./Piece";
import {ChessPiece, Move, PieceColor, PieceType} from "../types";


type BoardProps = {
    sendMove: (move: Move) => void,
    color: 'white' | 'black',
    moves: Array<Move>,
}

export default function Board(props: BoardProps) {

    useEffect(() => {
        if (props.moves.length === 0) {
            return;
        }
        const piece: ChessPiece | null = findPiece(props.moves[props.moves.length - 1].startIndex);
        if (piece === null) {
            return;
        }
        moveMove(props.moves[props.moves.length - 1], piece);
    }, [props.moves])

    const defaultSet: PieceType[] = [
        PieceType.rook,
        PieceType.knight,
        PieceType.bishop,
        PieceType.queen,
        PieceType.king,
        PieceType.bishop,
        PieceType.knight,
        PieceType.rook,
        PieceType.pawn,
        PieceType.pawn,
        PieceType.pawn,
        PieceType.pawn,
        PieceType.pawn,
        PieceType.pawn,
        PieceType.pawn,
        PieceType.pawn,
    ];

    useState();
    const [selectedIndex, setSelectedIndex] = useState<number | null>(null);
    const [pieces, setPieces] = useState<ChessPiece[]>([]);
    const [whiteTurn, setWhiteTurn] = useState<boolean>(true);
    const [possibleFields, setPossibleFields] = useState<number[]>([]);
    const [allPossibleMoves, setAllPossibleMoves] = useState<Move[]>([]);
    const [moves, setMoves] = useState<Move[]>([]);
    const [board, setBoard] = useState<any>(
        [
            PieceType.rook, PieceType.knight, PieceType.bishop, PieceType.queen, PieceType.king, PieceType.bishop, PieceType.knight, PieceType.rook, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceType.rook, PieceType.knight, PieceType.bishop, PieceType.queen, PieceType.king, PieceType.bishop, PieceType.knight, PieceType.rook, -1, -1, -1, -1, -1, -1, -1, -1,
        ]
    );

    const [colors, setColors] = useState<any>(
        [
            PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, -1, -1, -1, -1, -1, -1, -1, -1,
        ]
    );

    const SQUARES = {
        a8: 0, b8: 1, c8: 2, d8: 3, e8: 4, f8: 5, g8: 6, h8: 7,
        a7: 16, b7: 17, c7: 18, d7: 19, e7: 20, f7: 21, g7: 22, h7: 23,
        a6: 32, b6: 33, c6: 34, d6: 35, e6: 36, f6: 37, g6: 38, h6: 39,
        a5: 48, b5: 49, c5: 50, d5: 51, e5: 52, f5: 53, g5: 54, h5: 55,
        a4: 64, b4: 65, c4: 66, d4: 67, e4: 68, f4: 69, g4: 70, h4: 71,
        a3: 80, b3: 81, c3: 82, d3: 83, e3: 84, f3: 85, g3: 86, h3: 87,
        a2: 96, b2: 97, c2: 98, d2: 99, e2: 100, f2: 101, g2: 102, h2: 103,
        a1: 112, b1: 113, c1: 114, d1: 115, e1: 116, f1: 117, g1: 118, h1: 119
    };

    useEffect(() => {
        const tempPieces: ChessPiece[] = [];

        defaultSet.forEach(
            (type, index) => tempPieces.push({
                index,
                type,
                color: PieceColor.black,
                x: Math.floor(index / 8),
                y: index % 8,
            }));

        defaultSet[3] = PieceType.king;
        defaultSet[4] = PieceType.queen;

        defaultSet.reverse().forEach(
            (type, index) => tempPieces.push({
                index: index + 48,
                type,
                color: PieceColor.white,
                x: Math.floor((index + 48) / 8),
                y: (index + 48) % 8,
            }));

        setPieces(tempPieces);
    }, []);

    function generateMoves(): Array<Move> {
        const moves: Array<Move> = [];

        const currentColor = whiteTurn ? PieceColor.white : PieceColor.black;

        for (let current: number = SQUARES.a8; current <= SQUARES.h1; current++) {
            if (current & 0x88) {
                current += 7;
                continue;
            }

            if (board[current] === PieceType.pawn) {
                let step = (whiteTurn ? -16 : 16);
                let target: number = current + step;

                //one step
                if (!(target & 0x88) && !board[target]) {
                    moves.push({
                        startIndex: current,
                        targetIndex: target,
                        type: PieceType.pawn,
                        time: -1,
                    });
                }

                //two steps
                target = current + step;
                if (
                    !(target & 0x88)
                    && (whiteTurn ? current >= SQUARES.a2 : current <= SQUARES.h2)
                    && !board[target]
                ) {
                    moves.push({
                        startIndex: current,
                        targetIndex: target + step,
                        type: PieceType.pawn,
                        time: -1,
                    })
                }

                //kill
                step = (whiteTurn ? -17 : 17);
                target = current + step;
                if (!(target & 0x88) && board[target]) {
                    if (currentColor !== colors[target]) {
                        moves.push({
                            startIndex: current,
                            targetIndex: target,
                            type: PieceType.pawn,
                            time: -1,
                        });
                    }
                }
                step = (whiteTurn ? -15 : 15);
                target = current + step;
                if (!(target & 0x88) && board[target]) {
                    if (currentColor !== colors[target]) {
                        moves.push({
                            startIndex: current,
                            targetIndex: target,
                            type: PieceType.pawn,
                            time: -1,
                        });
                    }
                }

                //TODO en passant
            }

            if (board[current] === PieceType.knight) {
                [-18, -33, -31, -14, 18, 33, 31, 14].forEach(step => {
                    const target: number = current + step;
                    if (!(target & 0x88)) {
                        if (currentColor !== colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.knight,
                                time: -1,
                            });
                        }
                    }
                })
            }

            if (board[current] === PieceType.bishop) {
                [-17, 17, -15, 15].forEach(step => {
                    let target: number = current + step;
                    while (!(target & 0x88)) {
                        if (currentColor !== colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.bishop,
                                time: -1,
                            });
                        }

                        if (board[target]) {
                            break;
                        }

                        target += step;
                    }
                })
            }

            if (board[current] === PieceType.rook) {
                [-1, 1, -16, 16].forEach(step => {
                    let target: number = current + step;
                    while (!(target & 0x88)) {
                        if (currentColor !== colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.rook,
                                time: -1,
                            });
                        }

                        if (board[target]) {
                            break;
                        }

                        target += step;
                    }
                })
            }

            if (board[current] === PieceType.queen) {
                [-1, 1, -15, 15, -16, 16, -17, 17].forEach(step => {
                    let target: number = current + step;
                    while (!(target & 0x88)) {
                        if (currentColor !== colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.queen,
                                time: -1,
                            });
                        }

                        if (board[target]) {
                            break;
                        }

                        target += step;
                    }
                })
            }

            if (board[current] === PieceType.king) {
                [-1, 1, -15, 15, -16, 16, -17, 17].forEach(step => {
                    let target: number = current + step;
                    if (!(target & 0x88)) {
                        if (currentColor !== colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.king,
                                time: -1,
                            });
                        }
                    }
                })
            }
        }

        return moves;
    }

    const findPiece = (index: Number): ChessPiece | null => {
        return pieces.find(element => element.index === index) || null;
    };

    function pickPiece(index: number): void {
        const piece: ChessPiece | null = findPiece(index);

        if (piece === null) {
            return;
        }

        if (!isMyTurn(piece)) {
            return;
        }

        const generatedMoves = generateMoves();
        setAllPossibleMoves(generatedMoves);

        let index88 = piece.index + (piece.index & ~7);
        const filtered = generatedMoves.filter(el => {
            return el.startIndex === index88;
        });

        setPossibleFields(filtered.map(el => el.targetIndex));

        setSelectedIndex(index);
    }

    function isMyTurn(piece: ChessPiece): boolean {
        return ((whiteTurn && piece.color === PieceColor.white) ||
            (!whiteTurn && piece.color === PieceColor.black));
    }

    function getCords(index: number) {
        const x = Math.floor(index / 8);
        const y = index % 8;
        return {x, y};
    }

    function canMove(movedPiece: ChessPiece, targetIndex: number): boolean {
        return possibleFields.indexOf(targetIndex + (targetIndex & ~7)) != -1;
    }

    function moveMove(move: Move, piece: ChessPiece) {
        if (piece.index === move.targetIndex) {
            setSelectedIndex(null);
            return;
        }

        const tempPieces = [...pieces];
        const movedPiece = tempPieces.find(element => element.index === piece.index);

        if (movedPiece === undefined) {
            return;
        }

        const takenPiece = tempPieces.find(element => element.index === move.targetIndex);
        if (takenPiece) {
            if (takenPiece.color === piece.color) {
                return;
            }

            tempPieces.splice(tempPieces.indexOf(takenPiece), 1);
        }

        movedPiece.index = move.targetIndex;

        setPieces(tempPieces);

        setMoves([...moves, move]);

        setWhiteTurn(!whiteTurn);

        let tempBoard = [...board];
        tempBoard[move.startIndex + (move.startIndex & ~7)] = null;
        tempBoard[move.targetIndex + (move.targetIndex & ~7)] = piece.type;
        setBoard(tempBoard);

        let tempColors = [...colors];
        tempColors[move.startIndex + (move.startIndex & ~7)] = null;
        tempColors[move.targetIndex + (move.targetIndex & ~7)] = piece.color;
        setColors(tempColors);
    }

    function movePiece(pieceIndex: number, targetIndex: number) {
        const piece: ChessPiece | null = findPiece(pieceIndex);
        if (piece === null) {
            return;
        }

        if (!isMyTurn(piece)) {
            return false;
        }

        if (!canMove(piece, targetIndex)) {
            return false;
        }

        const move: Move = {
            type: piece.type,
            startIndex: piece.index,
            targetIndex,
            time: Date.now(),
        };

        moveMove(move, piece);

        props.sendMove(move);
    }

    function onPressHandler(fieldIndex: number): void {
        setPossibleFields([]);
        if (selectedIndex === null) {
            pickPiece(fieldIndex)
            return;
        }

        movePiece(selectedIndex, fieldIndex)
        setSelectedIndex(null);
        setPossibleFields([]);
    }

    return (
        <View
            style={{
                flex: 1,
                flexWrap: 'wrap',
                flexDirection: 'row',
                width: 400,
                backgroundColor: 'gray',
                alignContent: 'center',
                transform: [{rotate: props.color === 'white' ? '0' : '180deg'}],
            }}
        >
            {[...Array(64)].map((_, index) => {
                    let pieceElement = null;
                    const piece = findPiece(index);
                    if (piece !== null) {
                        pieceElement = (<Piece
                            rotate={props.color === 'white'}
                            piece={piece}/>);
                    }

                    return (
                        <TouchableOpacity
                            activeOpacity={.7}
                            onPress={() => onPressHandler(index)}
                            key={`field_${index}`}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: (
                                    (index % 16) === 0 ||
                                    (index % 16 - 2) === 0 ||
                                    (index % 16 - 4) === 0 ||
                                    (index % 16 - 6) === 0 ||
                                    (index % 16 - 9) === 0 ||
                                    (index % 16 - 11) === 0 ||
                                    (index % 16 - 13) === 0 ||
                                    (index % 16 - 15) === 0
                                ) ? '#936b1a' : '#d2cbcb',
                                width: 50,
                                height: 50,
                                borderColor: 'blue',
                                borderWidth: index === selectedIndex ? 3 : 0,
                            }}
                        >
                            {pieceElement}
                            <Text style={{
                                fontSize: 10,
                                position: "absolute",
                                textShadowColor: "white",
                                textShadowOffset: {width: 1, height: 1}
                                // }}>{index}({Math.floor(index / 8)}, {index % 8})</Text>
                            }}>
                                {/*({index + (index & ~7)})*/}
                            </Text>
                            {possibleFields.indexOf(index + (index & ~7)) != -1 ?
                                <Text style={{position: "absolute", bottom: 0, color: "red", fontSize: 50}}>▢</Text> : null}
                        </TouchableOpacity>
                    )
                }
            )}
            <View>
                {/*<Text>*/}
                {/*    whiteTurn: {whiteTurn ? 'true' : 'false'}*/}
                {/*</Text>*/}
                {/*<Text>selectedIndex: {selectedIndex}</Text>*/}
                {/*<Text>selectedX: {Math.floor(selectedIndex / 8)}</Text>*/}
                {/*<Text>selectedY: {selectedIndex % 8}</Text>*/}

                {/*<Text>Pieces count: {pieces.length}</Text>*/}
                {/*{pieces.map(piece => {*/}
                {/*    return (*/}
                {/*        <Piece key={`piece_${piece.index}`} piece={piece}/>*/}
                {/*    );*/}
                {/*})}*/}
            </View>
        </View>
    );
}
