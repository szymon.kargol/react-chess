import {ChessPiece, Move, PieceColor, PieceType} from "../types";

const SQUARES = {
    a8: 0, b8: 1, c8: 2, d8: 3, e8: 4, f8: 5, g8: 6, h8: 7,
    a7: 16, b7: 17, c7: 18, d7: 19, e7: 20, f7: 21, g7: 22, h7: 23,
    a6: 32, b6: 33, c6: 34, d6: 35, e6: 36, f6: 37, g6: 38, h6: 39,
    a5: 48, b5: 49, c5: 50, d5: 51, e5: 52, f5: 53, g5: 54, h5: 55,
    a4: 64, b4: 65, c4: 66, d4: 67, e4: 68, f4: 69, g4: 70, h4: 71,
    a3: 80, b3: 81, c3: 82, d3: 83, e3: 84, f3: 85, g3: 86, h3: 87,
    a2: 96, b2: 97, c2: 98, d2: 99, e2: 100, f2: 101, g2: 102, h2: 103,
    a1: 112, b1: 113, c1: 114, d1: 115, e1: 116, f1: 117, g1: 118, h1: 119
};

export default class Chess {
    board: Array<any> = [];
    colors: Array<any> = [];
    whiteTurn: boolean = true;

    currentMove: number = -1;

    boardHistory: Array<Array<any>> = [];
    colorsHistory: Array<Array<any>> = [];

    constructor() {
        //gameplay start
        this.board = [
            PieceType.rook, PieceType.knight, PieceType.bishop, PieceType.queen, PieceType.king, PieceType.bishop, PieceType.knight, PieceType.rook, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, PieceType.pawn, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceType.rook, PieceType.knight, PieceType.bishop, PieceType.queen, PieceType.king, PieceType.bishop, PieceType.knight, PieceType.rook, -1, -1, -1, -1, -1, -1, -1, -1,
        ];

        this.boardHistory.push([...this.board]);

        this.colors = [
            PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, PieceColor.black, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            null, null, null, null, null, null, null, null, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, -1, -1, -1, -1, -1, -1, -1, -1,
            PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, PieceColor.white, -1, -1, -1, -1, -1, -1, -1, -1,
        ];

        this.colorsHistory.push([...this.colors]);
    }

    pick(index: number): ChessPiece | null {
        if (this.board[index]) {
            return {
                index,
                type: this.board[index],
                color: this.colors[index],
                x: index & 7,
                y: index >> 7,
            }
        }

        return null;
    }

    /**
     * @param current x88
     * @param target x88
     */
    move(current: number, target: number) {
        if (current === target) {
            return;
        }

        if (this.whiteTurn) {
            if (this.colors[current] !== PieceColor.white) {
                return;
            }
            if (this.colors[target] === PieceColor.white) {
                return;
            }
        }

        if (!this.whiteTurn) {
            if (this.colors[current] !== PieceColor.black) {
                return;
            }
            if (this.colors[target] === PieceColor.black) {
                return;
            }
        }

        const generatedMoves = this.generateMoves();
        const movesFromCurrent = generatedMoves.filter(el => {
            return el.startIndex === current;
        });

        // const targets = movesFromCurrent.map(el => el.targetIndex);

        const movedPiece = this.board[current];
        // const targetPiece = this.board[target];

        this.board[current] = null;
        this.colors[current] = null;

        this.board[target] = movedPiece;
        this.colors[target] = this.whiteTurn ? PieceColor.white : PieceColor.black;

        this.whiteTurn = !this.whiteTurn;

        this.currentMove = this.boardHistory.length;

        this.boardHistory.push([...this.board]);
        this.colorsHistory.push([...this.colors]);
    }

    getCurrentMove(): number {
        return this.currentMove;
    }

    goTo(move: number) {
        if (move < 0) {
            return;
        }
        this.currentMove = move;
        this.board = [...this.boardHistory[move]];
        this.colors = [...this.colorsHistory[move]];
    }

    goToLastMove() {
        this.currentMove = this.boardHistory.length;
        this.board = [...this.boardHistory[this.boardHistory.length - 1]];
        this.colors = [...this.colorsHistory[this.boardHistory.length - 1]];
    }

    generateMoves(): Array<Move> {
        const moves: Array<Move> = [];

        const currentColor = this.whiteTurn ? PieceColor.white : PieceColor.black;

        for (let current: number = SQUARES.a8; current <= SQUARES.h1; current++) {
            if (current & 0x88) {
                current += 7;
                continue;
            }

            if (this.board[current] === PieceType.pawn) {
                let step = (this.whiteTurn ? -16 : 16);
                let target: number = current + step;

                //one step
                if (!(target & 0x88) && !this.board[target]) {
                    moves.push({
                        startIndex: current,
                        targetIndex: target,
                        type: PieceType.pawn,
                        time: -1,
                    });
                }

                //two steps
                target = current + step;
                if (
                    !(target & 0x88)
                    && (this.whiteTurn ? current >= SQUARES.a2 : current <= SQUARES.h2)
                    && !this.board[target]
                ) {
                    moves.push({
                        startIndex: current,
                        targetIndex: target + step,
                        type: PieceType.pawn,
                        time: -1,
                    })
                }

                //kill
                step = (this.whiteTurn ? -17 : 17);
                target = current + step;
                if (!(target & 0x88) && this.board[target]) {
                    if (currentColor !== this.colors[target]) {
                        moves.push({
                            startIndex: current,
                            targetIndex: target,
                            type: PieceType.pawn,
                            time: -1,
                        });
                    }
                }
                step = (this.whiteTurn ? -15 : 15);
                target = current + step;
                if (!(target & 0x88) && this.board[target]) {
                    if (currentColor !== this.colors[target]) {
                        moves.push({
                            startIndex: current,
                            targetIndex: target,
                            type: PieceType.pawn,
                            time: -1,
                        });
                    }
                }

                //TODO en passant
            }

            if (this.board[current] === PieceType.knight) {
                [-18, -33, -31, -14, 18, 33, 31, 14].forEach(step => {
                    const target: number = current + step;
                    if (!(target & 0x88)) {
                        if (currentColor !== this.colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.knight,
                                time: -1,
                            });
                        }
                    }
                })
            }

            if (this.board[current] === PieceType.bishop) {
                [-17, 17, -15, 15].forEach(step => {
                    let target: number = current + step;
                    while (!(target & 0x88)) {
                        if (currentColor !== this.colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.bishop,
                                time: -1,
                            });
                        }

                        if (this.board[target]) {
                            break;
                        }

                        target += step;
                    }
                })
            }

            if (this.board[current] === PieceType.rook) {
                [-1, 1, -16, 16].forEach(step => {
                    let target: number = current + step;
                    while (!(target & 0x88)) {
                        if (currentColor !== this.colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.rook,
                                time: -1,
                            });
                        }

                        if (this.board[target]) {
                            break;
                        }

                        target += step;
                    }
                })
            }

            if (this.board[current] === PieceType.queen) {
                [-1, 1, -15, 15, -16, 16, -17, 17].forEach(step => {
                    let target: number = current + step;
                    while (!(target & 0x88)) {
                        if (currentColor !== this.colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.queen,
                                time: -1,
                            });
                        }

                        if (this.board[target]) {
                            break;
                        }

                        target += step;
                    }
                })
            }

            if (this.board[current] === PieceType.king) {
                [-1, 1, -15, 15, -16, 16, -17, 17].forEach(step => {
                    let target: number = current + step;
                    if (!(target & 0x88)) {
                        if (currentColor !== this.colors[target]) {
                            moves.push({
                                startIndex: current,
                                targetIndex: target,
                                type: PieceType.king,
                                time: -1,
                            });
                        }
                    }
                })
            }
        }

        return moves;
    }
}
