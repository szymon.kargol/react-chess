export enum PieceType {
    'p' = 'p',
    'r' = 'r',
    'n' = 'n',
    'b' = 'b',
    'q' = 'q',
    'k' = 'k',
}

export enum PieceColor {
    'white' = 'white',
    'black' = 'black',
}

export type ChessPiece = {
    index: number;
    type: PieceType;
    color: PieceColor;
    x: number;
    y: number;
}

export type Move = {
    startIndex: number;
    targetIndex: number;
    type: PieceType;
    time: number,
}

export type Views = 'menu' | 'lobby' | 'game' | 'history';
