# Szachy

Autor: Szymon Kargol

Gra w szachy na urządzenia mobilne. 
Pozwala na prowadzenie rozgrywek z wybranym przeciwnikiem z Internetu.

---

## Wymagania funkcjonalne

### 1. Możliwości gracza

1. Zgłaszanie się do gry
   1. Wybór długości partii

2. Losowe parowanie z oponentem
   1. Losowe przyznanie koloru bierek

3. Rozgrywka partii szachów
   1. Wyświetlenie wykonanych ruchów 
   2. Wyświetlenie zegara 
   3. Wyświetlenie możliwych ruchów po wskazaniu bierki
   4. Wykonanie ruchu
   5. Czatowanie z przeciwnikiem
   6. Wyświetlenie finalnego wyniku gry

4. Wyświetlenie rozegranych gier
   1. Wyświetlenie wykonanych ruchów
   2. Przewijanie historii ruchów
