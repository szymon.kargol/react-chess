import React, {useEffect, useRef, useState} from 'react';
import {BackHandler, Button, Modal, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Move, Views} from "./src/types";
import Menu from "./src/components/Menu";
import Lobby from "./src/components/Lobby";
import ChessBoard from "./src/components/ChessBoard";
import HistoryBoard from "./src/components/HistoryBoard";
import History from "./src/components/History";

const ws = new WebSocket('ws://localhost:8999');

export default function App() {
    const [nameModalVisible, setNameModalVisible] = useState<boolean>(true);
    const [playerName, setPlayerName] = useState('');
    const [color, setColor] = useState<'w' | 'b'>('w');
    const [ready, setReady] = useState<boolean>(false);
    const [id, setId] = useState<number>(0);
    const [moves, setMoves] = useState<Array<Move>>([]);
    const [players, setPlayers] = useState<Array<string>>(['None']);
    const [games, setGames] = useState<Array<{ game_id: string, pgn: string }>>([]);
    const [view, setView] = useState<Views>('menu');
    const [alert, setAlert] = useState<{ text: string, params: object } | null>(null);
    const [gameId, setGameId] = useState<string | null>(null);
    const [game, setGame] = useState<any>(null);

    const boardRef = useRef();

    function addMove(move: Move) {
        setMoves([...moves, move]);
    }

    function addChat(json: any) {
        console.log(json);
    }

    function submitName() {
        setNameModalVisible(false);
        connect(playerName);
    }

    async function connect(name: string) {
        const tick = function () {
            ws.send(JSON.stringify({type: 'tick'}));

            setTimeout(tick, 5000);
        }

        if (!ws) {
            console.error('no ws')
            return;
        }

        ws.send(JSON.stringify({type: 'connect', name}));

        setTimeout(tick, 5000);

        ws.addEventListener('message', function (event) {
            const json = JSON.parse(event.data);
            console.log('Message from server:', json);
            switch (json.type) {
                case 'connect':
                    setId(json.id);
                    setReady(true);
                    break;
                case 'invite':
                    setAlert({
                        text: `Do you want to play with ${json.name}?`,
                        params: {type: 'accept', game_id: json.game_id}
                    });
                    break;
                case 'move':
                    // addMove(json.move);
                    // @ts-ignore
                    boardRef.current.rec(json.move)
                    // @ts-ignore
                    boardRef.current.clock(json.clock)
                    break;
                case 'chat':
                    addChat(json.chat);
                    break;
                case 'players':
                    setPlayers(json.players);
                    break;
                case 'games':
                    setGames(json.games);
            }
        });
    }

    useEffect(() => {
    }, []);

    function sendMoveHandler(move: string, pgn: string) {
        if (!ws) {
            console.error('no ws')
            return;
        }
        ws.send(JSON.stringify({
            type: 'move',
            game_id: gameId,
            id,
            move,
            pgn,
        }))
    }

    function inviteHandler(name: string|null, clock: number) {
        if (!ws) {
            console.error('no ws')
            return;
        }
        ws.send(JSON.stringify({
            type: 'invite',
            name,
            clock,
        }))
    }

    function play(game: any) {
        setGameId(game.game_id);
        setColor(game.player_one === id ? game.one_color : game.two_color)
        setGame(game)
        setView('game');
    }

    function getView(view: Views) {
        switch (view) {
            case 'menu':
                return <Menu navigate={(view: Views) => setView(view)}/>;
            case 'lobby':
                return <Lobby
                    playerName={playerName}
                    players={players}
                    games={games}
                    invite={inviteHandler}
                    navigate={(view: Views) => setView(view)}
                    play={(game: any) => play(game)}/>
            case 'game':
                if (!ready) {
                    return (<Text>Loading</Text>);
                }
                return (<ChessBoard
                    sendMove={(move: string, pgn: string) => sendMoveHandler(move, pgn)}
                    ref={boardRef}
                    game={game}
                    id={id}
                    color={color}/>);
            case 'history':
                return (<History games={games}
                                 players={players}/>);
                // return (<HistoryBoard pgn={''}/>);
            // return (<Text>History</Text>);
        }
    }

    function getAlert() {
        if (alert === null) {
            return null;
        }

        return (
            <View style={styles.modal}>
                <Text style={styles.alertText}>{alert.text}</Text>
                <View style={styles.alertButtons}>
                    <Button onPress={() => {
                        if (alert.params) ws.send(JSON.stringify(alert.params));
                        setAlert(null);
                    }} title={'Accept'}/>
                    <Button onPress={() => {
                        // if (alert.params) ws.send(JSON.stringify(alert.params));
                        setAlert(null);
                    }} title={'Reject'}/>
                </View>
            </View>
        );
    }

    useEffect(() => {
        const backAction = () => {
            setView('menu');
            return true;
        };

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );

        return () => backHandler.remove();
    }, []);

    return (
        <View style={styles.container}>
            <View style={{width: '96%', paddingLeft: '2%', paddingRight: '2%', marginTop: '6%'}}>
                <Text style={styles.title}>♞ React Chess</Text>
                {view !== 'menu' ?
                    <TouchableOpacity style={styles.menu}
                                      onPress={() => setView('menu')}><Text
                        style={styles.title}>Menu ☰</Text></TouchableOpacity> : null}
            </View>
            {getAlert()}
            <Modal
                visible={nameModalVisible}
                animationType={'slide'}
                transparent={true}
            >
                <View style={styles.modal}>
                    <Text>Enter user name:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={text => setPlayerName(text)}
                    />
                    <Button title={'Connect'} onPress={submitName}/>
                </View>
            </Modal>
            {getView(view)}
        </View>
    );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#135d8f',
        marginBottom: 20,
    },
    menu: {
        position: "absolute",
        right: 0,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    alert: {
        backgroundColor: '#fff',
        padding: 10,
    },
    alertText: {
        color: 'white',
        marginBottom: 10,
    },
    alertButtons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    input: {
        borderWidth: 1,
        marginBottom: 10,
        fontSize: 20,
    },
    modal: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        margin: 'auto',
        backgroundColor: 'rgb(150,150,229)',
        padding: 20,
        borderRadius: 4,
    }
});
