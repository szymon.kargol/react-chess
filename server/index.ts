import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import * as crypto from "crypto";
import * as sqlite3 from 'sqlite3'
import {open} from 'sqlite'
import * as path from "path";

// @ts-ignore
const app = express();

const server = http.createServer(app);

const wss = new WebSocket.Server({server});

(async () => {
    const db = await open({
        filename: path.resolve(__dirname, 'sqlite'),
        driver: sqlite3.Database
    })

    // await db.exec('CREATE TABLE games (game_id VARCHAR, player_one VARCHAR, player_two VARCHAR, moves TEXT)');
    /*
    alter table games
        add game_id VARCHAR;

    alter table games
        add player_one VARCHAR;

    alter table games
        add player_two VARCHAR;

    alter table games
        add moves TEXT;

    alter table games
        add accepted boolean default false;

    alter table games
        add one_color VARCHAR;

    alter table games
        add two_color VARCHAR;

    alter table games
        add pgn TEXT;


     */
    const players: any[] = [];

    function broadcast(message: any) {
        players.forEach(player => {
            player.ws.send(JSON.stringify(message));
        })
    }

    function broadcastPlayers() {
        broadcast({
            type: 'players',
            players: players.map(player => {
                return {
                    id: player.id,
                    name: player.name
                }
            }),
        });
    }

    wss.on('connection', (ws: WebSocket) => {
        ws.send(JSON.stringify({message: 'Welcome to the server!'}));

        const send = (data: Object) => {
            ws.send(JSON.stringify(data));
        }

        const id = crypto.randomUUID();
        console.log('connection', id);

        ws.on('message', async (message: string) => {
            const result = await db.all(
                'SELECT * FROM games WHERE accepted=1 AND player_one=? OR player_two=?',
                id, id
            )
            ws.send(JSON.stringify({
                type: 'games',
                games: result,
            }));

            const json = JSON.parse(message);
            console.log(json);
            let newPlayer: null | {
                id: string,
                name: string,
                color: string,
                ws: WebSocket,
            } = null;
            switch (json.type) {
                case 'connect':
                    newPlayer = {
                        id,
                        name: json.name,
                        color: 'white',
                        ws,
                    };
                    console.log(id);
                    players.push(newPlayer);
                    broadcastPlayers();
                    send({
                        type: 'connect',
                        id: newPlayer.id,
                        color: players.length === 1 ? 'white' : 'black',
                    });
                    break;
                case 'invite': {
                    //validate if is in progress or waiting for accept
                    const opponent = players.find(player => {
                        return player.name === json.name;
                    });
                    const currentPlayer = players.find(player => player.id === id);
                    const newGameId = crypto.randomUUID();
                    if (opponent && currentPlayer) {
                        opponent.ws.send(JSON.stringify({
                            type: 'invite',
                            name: currentPlayer.name,
                            game_id: newGameId,
                        }))
                        const one_color = Math.floor(Math.random() * 2) ? 'w' : 'b';
                        const two_color = one_color === 'w' ? 'b' : 'w';
                        const clock = json.clock
                        const result = await db.run(
                            'INSERT INTO games (game_id, player_one, player_two, one_color, two_color, clock, one_time, two_time, one_move_time, two_move_time) VALUES (?,?,?,?,?,?,?,?,?,?)',
                            newGameId, id, opponent.id, one_color, two_color, clock, clock, clock, Date.now(), Date.now()
                        )
                    }
                }
                    break;
                case 'accept': {
                    const result = await db.run(
                        'UPDATE games SET accepted = true, moves = JSON_SET(?) WHERE game_id = ?',
                        [JSON.stringify([]), json.game_id]
                    )
                    console.log('accept result', result);
                }
                    break;
                case 'move':
                    const game = await db.all(
                        'SELECT * FROM games WHERE game_id = ?',
                        [json.game_id]
                    )
                    let {one_time, two_time, one_move_time, two_move_time} = game[0];
                    const isOne = (game[0].player_one === id)
                    one_time = isOne ? (one_time - (Date.now() - one_move_time)) : one_time;
                    two_time = !isOne ? (two_time - (Date.now() - two_move_time)) : two_time;
                    const result = await db.run(
                        `UPDATE games
                         SET moves         = JSON_INSERT(moves, '$[#]', ?),
                             pgn           = ?,
                             one_time      = ?,
                             two_time      = ?,
                             one_move_time = ?,
                             two_move_time = ?
                         WHERE game_id = ?`,
                        [
                            JSON.stringify(json.move),
                            json.pgn,
                            one_time,
                            two_time,
                            isOne ? Date.now() : one_move_time,
                            !isOne ? Date.now() : two_move_time,
                            json.game_id,
                        ],
                    )
                    console.log('move', result);
                    const opponent = players.find(p => {
                        return p.id != json.id;
                    });
                    if (!opponent) break;
                    opponent.ws.send(JSON.stringify({
                        type: 'move',
                        move: json.move,
                        clock: {one_time, two_time}
                    }));
                    break;
            }
        });

        ws.on('close', () => {
            console.log('close', id);
            const index = players.findIndex(player => player.id === id);
            if (index >= 0) {
                players.splice(index, 1);
                broadcastPlayers();
            }
        });
    });

    server.listen(process.env.PORT || 8999, () => {
        // @ts-ignore
        console.log(`Server started on port ${server.address().port} :)`);
    });

})
()
